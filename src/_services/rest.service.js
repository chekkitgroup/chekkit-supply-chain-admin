import axios from "axios";
import { config } from '../config';

class RestService { 

  getAllCompanies(page, per_page) {
    return this.DoGet(`admin/companies?page=${page}&perPage=${per_page}`)
  }

  getCompanyUsers(id) {
    return this.DoGet(`admin/companies/${id}/users`)
  }

  getHardware(id, page, per_page) {
    return this.DoGet(`admin/company/${id}/hardwares?page=${page}&perPage=${per_page}`)
  }

  getAssets(id, page, per_page) {
    return this.DoGet(`admin/company/${id}/assets?page=${page}&perPage=${per_page}`)
  }

  getAllRfidTags() {
    return this.DoGet('admin/rfids')
  }

  gatewayProvision(data) {
    return this.DoPut("admin/gateways/assign",data)
  }

  getUnassignedGateway() {
    return this.DoGet('admin/gateways/unassigned')
  }

  deprovisionGateway(macAddress) {
    return this.DoDelete(`admin/gateways/de-provision/${macAddress}`)
  }

  registerTags(data) {
    return this.DoPost(`admin/rfids`,data)
  }

  getCompanyRoles(id) {
    return this.DoGet(`admin/company/${id}/roles`)
  }

  getCompanySubscriptions(id) {
    return this.DoGet(`admin/company/${id}/subscriptions`)
  }

  getCompanyPermissions(id) {
    return this.DoGet(`admin/company/${id}/permissions`)
  }

  createRoles(id, data) {
    return this.DoPost(`admin/company/${id}/create-role`,data)
  }

  ApproveSubscription(id, subId) {
    return this.DoPut(`admin/company/${id}/approve-subscription/${subId}`)
  }

  AssignTags(id, payload) {
    return this.DoPut(`admin/rfids/company/${id}/assign`,payload)
  }


  
  login(email, password) {
    return axios
      .post(config.apiGateway.BASE_URL + "auth/signin", {
        email,
        password
      })
      .then(response => {
        // console.log(response.data.data)
        if (response.data.data.token) {
          localStorage.setItem("chekkitsupplychainUser", JSON.stringify(response.data.data));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem("chekkit-su");
  }

  register(username, email, password) {
    return axios.post(config.apiGateway.BASE_URL + "auth/signup", {
      username,
      email,
      password
    });
  }

  isAuthenticated(token) {
    if (token) return true;
  }

  getCurrentUser() {
    let u = JSON.parse(localStorage.getItem('chekkitsupplychainUser'));
    return u
  }

  DoDelete(url){
    let u = JSON.parse(localStorage.getItem('chekkitsupplychainUser'));
    var token = '';
    if(this.isAuthenticated && u){
      token = u.token;
    }
    // console.log(u)
    return axios
      .delete(config.apiGateway.BASE_URL + url, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      })
      .then(response => {
        return response.data;
      });
  }

  DoEdit(url,payload){
    let u = JSON.parse(localStorage.getItem('chekkit-su'));
    var token = '';
    if(this.isAuthenticated && u){
      token = u.token;
    }

    let payloadObj = {};
     
    for(var pair of payload.entries()){
      payloadObj[pair[0]] = pair[1];
    }

    // console.log(u)
    return axios
      .put(config.apiGateway.BASE_URL + url,payloadObj, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      })
      .then(response => {
        return response.data;
      });
  }

  DoGet(url){
    let u = JSON.parse(localStorage.getItem('chekkitsupplychainUser'));
    // console.log('fgfggf', u)
    // console.log('fgfggf', this.isAuthenticated )
   
    var token = '';
    if(this.isAuthenticated && u){
      token = u.token;
    }

    return axios
    .get(config.apiGateway.BASE_URL + url, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      })
      .then(response => {
        return response.data;
      });

  }


  DoPost(url,payload){ 
    let u = JSON.parse(localStorage.getItem('chekkitsupplychainUser'));
    var token = '';
    if(this.isAuthenticated && u){
      token = u.token;
    }

    // console.log(u)
    return axios
      .post(config.apiGateway.BASE_URL + url,payload, {
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        }
      })
      .then(response => {
        return response.data;
      });

  }

  DoPostFormData(url,payload){
    let u = JSON.parse(localStorage.getItem('chekkit-su'));
    var token = '';
    if(this.isAuthenticated && u){
      token = u.token;
    }

    let payloadObj = {};
    console.log(payload)
    for(var pair of payload.entries()){
      payloadObj[pair[0]] = pair[1];
    }

    // console.log(u)
    return axios
      .post(config.apiGateway.BASE_URL + url,payloadObj, {
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        }
      })
      .then(response => {
        return response.data;
      });

  }

  DoPut(url,payload){
    let u = JSON.parse(localStorage.getItem('chekkitsupplychainUser'));
    var token = '';
    if(this.isAuthenticated && u){
      token = u.token;
    }

    return axios
      .put(config.apiGateway.BASE_URL + url,payload, {
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        }
      })
      .then(response => {
        return response.data;
      });

  }

  DoPostFile(url,payload){
    let u = JSON.parse(localStorage.getItem('chekkit-su'));
    var token = '';
    if(this.isAuthenticated && u){
      token = u.token;
    }

    return axios.post(config.apiGateway.BASE_URL + url, payload, {
      headers: {
        'content-type': 'multipart/form-data',
        'Authorization': `Bearer ${token}`
      }
    }).then(res => {
        return res.data;
      })
      .catch(err => {
        return err;
      })

  }

}

const myInstance = new RestService();
export default myInstance;

// export default new RestService();
 