// import React, { Component } from "react";
// import { Link } from 'react-router-dom';
// import Form from "react-validation/build/form";
// import AuthService from '../../../_services/authentication.service';
// import {
//   CButton,
//   CCard,
//   CCardBody,
//   CCardFooter,
//   CCol,
//   CContainer,
//   CForm,
//   CInput,
//   CInputGroup,
//   CInputGroupPrepend,
//   CInputGroupText,
//   CRow
// } from '@coreui/react'
// import CIcon from '@coreui/icons-react' 

// export default class Register extends Component {
//   constructor(props) {
//     super(props);
//     this.handleLogin = this.handleLogin.bind(this);
//     this.onChangeName = this.onChangeName.bind(this);
//     this.onChangePassword = this.onChangePassword.bind(this);
//     this.onChangeCompanyName = this.onChangeCompanyName.bind(this);
//     this.onChangeCountry = this.onChangeCountry.bind(this);
//     this.onChangeCompanyRole = this.onChangeCompanyRole.bind(this);
//     this.onChangeCompanyIdentifier = this.onChangeCompanyIdentifier.bind(this);
//     this.onChangeAddress = this.onChangeAddress.bind(this);
//     this.onChangeEmail = this.onChangeEmail.bind(this);
    

//     this.state = {
//       name: '', email: '', password: '', companyName: '', address: '', country: '', companyRole: '', companyIdentfier: '',
//       loading: false,
//       message: ""
//     };
//   }

//   onChangeName(e) {
//     this.setState({
//       name: e.target.value
//     });
//   }

//   onChangeEmail(e) {
//     this.setState({
//       email: e.target.value
//     });
//   }

//   onChangePassword(e) {
//     this.setState({
//       password: e.target.value
//     });
//   }

//   onChangeCompanyName(e) {
//     this.setState({
//       companyName: e.target.value
//     });
//   }

//   onChangeAddress(e) {
//     this.setState({
//       address: e.target.value
//     });
//   }

//   onChangeCountry(e) {
//     this.setState({
//       country: e.target.value
//     });
//   }

//   onChangeCompanyRole(e) {
//     this.setState({
//       companyRole: e.target.value
//     });
//   }

//   onChangeCompanyIdentifier(e) {
//     this.setState({
//       companyIdentfier: e.target.value
//     });
//   }

//   handleLogin(e) {
//     e.preventDefault();

//     this.setState({
//       message: "",
//       loading: true
//     });


//       AuthService.register(this.state.name, this.state.email, this.state.password, this.state.companyName, this.state.address, this.state.country, this.state.companyRole, this.state.companyIdentfier ).then(
//         () => {
//           this.props.history.push("/login");
//           // window.location.reload();
//         },
//         error => {
//           const resMessage =
//             (error.response &&
//               error.response.data &&
//               error.response.data.message) ||
//             error.message ||
//             error.toString();

//           this.setState({
//             loading: false,
//             message: resMessage
//           });
//         }
//       );
//   }


//   render() {

  
//   return (
//     <div className="c-app c-default-layout flex-row align-items-center">
//       <CContainer>
//         <CRow className="justify-content-center">
//           <CCol md="9" lg="7" xl="6">
//             <CCard className="mx-4">
//               <CCardBody className="p-4">
//               <Form
//                       onSubmit={this.handleLogin}
//                       ref={c => {
//                         this.form = c;
//                       }}
//                     >
//                   <h1>Register</h1>
//                   <p className="text-muted">Create your account</p>


//                   <CInputGroup className="mb-3">
//                     <CInput type="text" placeholder="Name" value={this.state.name} onChange={this.onChangeName}  />
//                   </CInputGroup>

//                   <CInputGroup className="mb-3">
//                     <CInput type="text" placeholder="Email" value={this.state.email} onChange={this.onChangeEmail}  />
//                   </CInputGroup>

//                   <CInputGroup className="mb-3">
//                     <CInput type="password" placeholder="Password" value={this.state.password} onChange={this.onChangePassword}  />
//                   </CInputGroup>

//                   <CInputGroup className="mb-3">
//                     <CInput type="text" placeholder="Company Name" value={this.state.companyName} onChange={this.onChangeCompanyName} />
//                   </CInputGroup>

//                   <CInputGroup className="mb-3">
//                     <CInput type="text" placeholder="Address" value={this.state.address} onChange={this.onChangeAddress}  />
//                   </CInputGroup>

//                   <CInputGroup className="mb-3">
//                     <CInput type="text" placeholder="Country" value={this.state.country} onChange={this.onChangeCountry}  />
//                   </CInputGroup>

//                   <CInputGroup className="mb-3">
//                     <CInput type="text" placeholder="Company Role"  value={this.state.companyRole} onChange={this.onChangeCompanyRole} />
//                   </CInputGroup>

//                   <CInputGroup className="mb-3">
//                     <CInput type="text" placeholder="Company Identifier" value={this.state.companyIdentfier} onChange={this.onChangeCompanyIdentifier} />
//                   </CInputGroup>
// {/* 
//                   <CInputGroup className="mb-3">
//                     <CInput type="text" placeholder="Role" value={this.state.roleId} onChange={this.onChangeRoleId} />
//                   </CInputGroup> */}

//                     <CRow>
//                         <CCol xs="12">
//                           <button
//                             className="btn btn-success btn-block"
//                             disabled={this.state.loading}
//                           >
//                             {this.state.loading && (
//                               <span className="spinner-border spinner-border-sm"></span>
//                             )}
//                             <span>Login</span>
//                           </button>
//                         </CCol>
//                       </CRow>
//                         {this.state.message && (
//                           <div className="form-group">
//                             <div className="alert alert-danger" role="alert">
//                               {this.state.message}
                             
//                             </div>
//                           </div>
//                         )}
//                  </Form>
                
//               </CCardBody>
              
//             </CCard>
//           </CCol>
//         </CRow>
//       </CContainer>
//     </div>
//   )
// }

// }