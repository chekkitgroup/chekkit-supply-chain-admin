import React, { Component } from "react";
import {
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import AuthService from '../../../_services/authentication.service';
import Form from "react-validation/build/form";



export default class Login extends Component {
  constructor(props) {
    super(props); 
    this.handleLogin = this.handleLogin.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);

    this.state = {
      email: "",
      password: "",
      loading: false,
      message: ""
    };
  }



  onChangeEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    });
  }

  handleLogin(e) {
    e.preventDefault();

    this.setState({
      message: "",
      loading: true
    });


      AuthService.login(this.state.email, this.state.password).then(
        () => {
          this.props.history.push("/dashboard");
          window.location.reload();
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
            console.log(error)

          this.setState({
            loading: false,
            message: resMessage
          }); 
        }
      );
  }

  render() {

    return (
      <div style={{ backgroundColor: "#03303b"}} className="c-app c-default-layout flex-row align-items-center">
        <CContainer>
          <CRow className="justify-content-center">
            <CCol md="8">
              <CCardGroup>
                
                <CCard className="p-4">
                  
                  <CCardBody >
                    
                    <Form 
                      onSubmit={this.handleLogin}
                      ref={c => {
                        this.form = c;
                      }}
                    >
                      <h1  className="text-3xl text-center font-bold">Login</h1>
                      <p className="text-muted text-small font-italic mb-2">Sign In to your account</p>
                      <CInputGroup className="mb-3">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <CIcon name="cil-user" />
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <CInput type="text" value={this.state.email} onChange={this.onChangeEmail} placeholder="Username" autoComplete="email" />
                      </CInputGroup>
                      <CInputGroup className="mb-4">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <CIcon name="cil-lock-locked" />
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <CInput type="password" value={this.state.password} onChange={this.onChangePassword} placeholder="Password" autoComplete="Password" />
                      </CInputGroup>
                      <CRow>
                        <CCol xs="12">
                          <button
                          style={{ backgroundColor: "#03303b", color:"#fff"}}
                            className="btn btn-default btn-block hover:bg-sky-600"
                            disabled={this.state.loading}
                          >
                            {this.state.loading && (
                              <span className="spinner-border spinner-border-sm"></span>
                            )}
                            <span>Login</span>
                          </button>
                        </CCol>
                      </CRow>
                        {this.state.message && (
                          <div className="form-group">
                            <div className="alert alert-danger" role="alert">
                              {this.state.message}
                             
                            </div>
                          </div>
                        )}
                    </Form>
                  </CCardBody>
                </CCard>
              </CCardGroup>
            </CCol>
          </CRow>
        </CContainer>
      </div>
    )
  } 
}

