import React, { useState, useEffect } from 'react';
import RestService from '../../_services/rest.service';

import { 
    // CRow, 
    // CContainer, 
    CDataTable,
    // CButtonGroup,
    // CButton, CModal,  
    // CModalBody, 
    // CModalHeader,
    // CLabel,
    // CFormGroup,
    // CModalFooter,   CCard, 
    // CCardBody, CForm, CInput,
    // CCol 
} from '@coreui/react'


const Plans = () => {
    const [plans, setPlans] = useState([]);

    useEffect(() => {
        loadData();
    },[])

    const loadData = async () => {
        RestService.getAllCompanies().then(
            (res) => {
                setPlans(res.data);
                console.log(res.data)
            },
            error => {
                // const resMessage =
                // (error.response &&
                //     error.response.data &&
                //     error.response.data.message) ||
                // error.message ||
                // error.toString();
                // console.log(error)
                // this.setState({
                // loading: false,
                // message: resMessage
                // });
            }
            );
    } 
  

    return (
        <div>
            <h1>This is plans</h1>
            <CDataTable items={plans}
                        fields={[
                            {label: 'Plan Name', key: 'name'},
                            //  {label: 'Price', key: 'price'}, {label: 'Trial Days', key: 'trail_days'}, {label: 'Date Created', key: 'created_at'}, {label: 'Actions', key: 'but'}
                        ]} hover striped itemsPerPage={100} clickableRows 
                            scopedSlots = {
                            {
                                'but':
                                    (item)=>(
                                      <td>
                                         {/* <CButtonGroup>  
                                            <CButton color="success" className="m-1" onClick={() => onEdit(item)}>Edit</CButton> 
                                            <CButton color="danger" className="m-1" onClick={() => onDeleteFunc(item.id)}>Delete</CButton> 
                                        </CButtonGroup> */}
                                          
                                  </td>
                            ),
                            }
                        }
                    /> 
        </div>
    )
}

export default Plans;




