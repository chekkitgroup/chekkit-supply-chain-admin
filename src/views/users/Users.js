import React, { Component } from 'react'

import { useHistory, useLocation } from 'react-router-dom'
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CPagination
} from '@coreui/react'

import RestService from '../../_services/rest.service';

const getBadge = status => {
  switch (status) {
    case true: return 'success'
    case false: return 'secondary'
    case 'Pending': return 'warning'
    case 'Banned': return 'danger'
    default: return 'primary'
  }
}
export default class Users extends Component {
  constructor(props) {
    super(props);
    this.pageChange = this.pageChange.bind(this);
    // this.onChangeUsername = this.onChangeUsername.bind(this);
    // this.onChangePassword = this.onChangePassword.bind(this);
    // const history = useHistory()

    // const pageChange = newPage => {
    //   currentPage !== newPage && history.push(`/users?page=${newPage}`)
    // }

    this.state = {
      users: [],
      username: "",
      currentPage: "1",
      password: "",
      loading: false,
      message: "",
      history: undefined,
    };
  }
  componentWillReceiveProps = (nextProps) => {
    console.log(232)
    console.log(nextProps)
    if (nextProps.currentPage !== this.props.currentPage) {
      this.setPage(nextProps.currentPage)
      // this.moveMap(nextProps.position)
    }
  }

  // useEffect(() => {
  //   currentPage !== page && setPage(currentPage)
  // }, [currentPage, page])


  componentDidMount() {
    this.getUsers()
    // this.useHistoryCall()
  }

  getUsers() {

    this.setState({
      message: "",
      loading: true
    });

    // this.form.validateAll();

      RestService.getAllUsers().then(
        (res) => {
          // console.log(res.data)


            this.setState({
              users: res.data,
            });
          // this.state = {users: res.data};

          // console.log(this.state.users)
          // this.props.history.push("/profile");
          // window.location.reload();
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            loading: false,
            message: resMessage
          });
        }
      );
  }

  setPage(){
    console.log('newPage')
    const queryPage = useLocation().search.match(/page=([0-9]+)/, '')
    const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1)
    // const [page, setPage] = useState(currentPage)
  }
  useHistoryCall(){    

    this.setState({
      history: useHistory(),
    });

    // const history = useHistory()
    // return history;
  }
  pageChange(newPage) {
    this.state.currentPage !== newPage && this.props.history.push(`/users?page=${newPage}`)
  }
// const Users = () => {

  // useEffect(() => {
  //   currentPage !== page && setPage(currentPage)
  // }, [currentPage, page])
 
  // fields={[
  //   { key: 'name', _classes: 'font-weight-bold' },
  //   'registered', 'role', 'status'
  // ]}
  render() {
    var _this = this;
    if(this.state.users) {          
      // console.log(this.state.users)

      return (
        <CRow>
          <CCol xl={12}>
            <CCard>
              {/* <CCardHeader>
                Users 
              </CCardHeader> */}
              <CCardBody>
              <CDataTable
                items={this.state.users}
                fields={[
                  { key: 'username', _classes: 'font-weight-bold' },
                  'email',{label: 'role', key: 'membership_type'}, 'verified'
                ]}
                hover
                striped
                itemsPerPage={5}
                activePage={this.state.page}
                clickableRows
                // onRowClick={(item) => this.useHistoryCall.push(`/users/${item.id}`)}
                scopedSlots = {{
                  'verifieda':
                    (item)=>(
                      <td>
                        <CBadge color={getBadge(item.verified)}>
                          {item.verified}
                        </CBadge>
                      </td>
                    )
                }}
              />
              <CPagination
                activePage={this.state.page}
                onActivePageChange={this.pageChange}
                pages={5}
                doubleArrows={false} 
                align="center"
              />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      )
    }
  } 

}
