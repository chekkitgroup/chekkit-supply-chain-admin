import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useParams } from "react-router-dom"
import RestService from '../../_services/rest.service';

import { 
    CButton
} from '@coreui/react'


const ViewHardware = () => {
    const [hardware, setHardware] = useState();
    const { id } = useParams()
    const history = useHistory()

    const navigate = (id) => {
        history.push('/companies/ViewCompanies')
    }



    useEffect(() => {
        loadData(id);
    },[id])



    const loadData = async (id) => {
        RestService.getHardware(id,1,100).then(
            (res) => {
                setHardware(res.data);
                console.log(res.data)
            },
            error => {
                
            }
            );
    } 

    
  

    return (
 
        
        <div>
            <div className="card">
            <table className="table table-striped table-hover">
                <tbody>
                <tr>
                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Name</th>
                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Quantity</th>
                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Order ID</th>
                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Order Date</th>
                </tr>
                {hardware && 
                    hardware.map((data, idx) => {
                        var cts = data.order_date,
                        cdate = (new Date(cts)).toLocaleString();
                        return (
                            <tr key={idx}>
                                <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.hardwareOrdersDetails[0].name}</td>
                                <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.hardwareOrdersDetails[0].quantity}</td>
                                <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.companyId}</td>
                                <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{cdate}</td>
                            </tr>
                        )
                    })
                }
                </tbody>
            </table>
            <div>
                    <CButton style={{ backgroundColor: "#03303b", color:"#fff"}} shape="rounded-0" size="lg" className="m-1" onClick={() => navigate(id)} >Back</CButton>
            </div>
            </div>        
        </div>
    )
}

export default ViewHardware;




