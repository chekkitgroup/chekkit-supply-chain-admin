import React, { useState, useEffect } from 'react'
import { useParams } from "react-router-dom"
import Select from 'react-select'

import { CRow, 
  CContainer, 
  CForm, 
  CFormGroup, 
  CLabel, 
  CCard, 
  CInput, 
  CCardBody,
  CCol, 
 } from '@coreui/react'
 import Notification, { notifier } from 'react-toast-notifier';
import RestService from '../../_services/rest.service';

const GateWayProvision = () => {
    const { id } = useParams()
    const [company, setCompany] = useState([]);
    const [selectCompany, setSelectCompany] = useState();
    const [formData, setFormData] = useState({
        companyId: '',
        location: '',
        macAddress: '',
        // latitude: '',
        // projectId: '',
        // gatewayType: '',
        deviceType: ''
    });

    console.log('selectCompany', selectCompany)


    useEffect(() => {
        loadData(id);
    },[id])

    const loadData = async () => {
        RestService.getAllCompanies(1, 15).then(
          
            (res) => {
                setCompany(res.data);
                console.log(res.data)

                // let companyId = res.data[0].id

                // console.log(companyId)
            },
            error => {
              console.log(error)
            }
            );
    } 


    
    const handleInputChange = e => {
      setFormData({...formData, [e.target.name]: e.target.value})
    };


      const onSelectCompany = async (data) => {
        console.log(data)
        setSelectCompany(data.value)
        let companyId = data.value;
        setFormData({...formData, companyId})
        console.log(data.value)
      }


    const clearForm = async () => {
        setFormData({
          companyId: '',
          location: '',
          macAddress: '',
          // latitude: '',
          // projectId: '',
          // gatewayType: '',
          deviceType: ''
        });
    }

    let note = () => {
      notifier({ type: "success", message: "Provisioned Successfully", autoDismissTimeout:5000 });
    } 

    const handleSubmit = e => {
        e.preventDefault()
        console.log(formData)

        RestService.gatewayProvision(formData).then(
            (res) => {
                // alert('Created Successfully')
                note()
                clearForm();
            },
            error => {
                console.log(error)
                // alert(error)
                notifier(error)

            }
        );
    } 


    return(
        <CContainer>
          <CRow>
            <CCol xs="10" sm="10" md="10" lg="10">
            <CCard className="mx-4">
              <CCardBody className="p-4">   
              <Notification />

              <CForm onSubmit={handleSubmit}>
                  <CFormGroup>
                  <label>Company</label>
                    {company && company.length > 0 && (
                        <Select name="companyId"  onChange={onSelectCompany} closeMenuOnSelect={true}  options={company.map(com => {
                                  return {value: com.id, label: com.name} 
                            })} 
                        />
                        )}
                  </CFormGroup>
                  {/* <CFormGroup>
                    <CLabel>Gateway Type</CLabel>
                    <CSelect name="deviceType" onChange={handleInputChange} value={formData.device}>
                        <option  value="wifi">WIFI</option>
                        <option value="lte">LTE</option>
                    </CSelect>
                  </CFormGroup> */}
                   <CFormGroup>
                    <CLabel>Mac Address</CLabel>
                    <CInput
                      type="text"
                      placeholder="Mac Address"
                      name="macAddress" 
                      value={formData.macAddress}
                      onChange={handleInputChange}
                      required
                    />
                  </CFormGroup>
                  <CFormGroup>
                    <CLabel>Location</CLabel>
                    <CInput
                      type="text"
                      placeholder="Location"
                      name="location" 
                      value={formData.location}
                      onChange={handleInputChange}
                      required
                    />
                  </CFormGroup>
                  {/* <PlacesAutocomplete
                      value={formData.location}
                      onChange={handleInputChange}
                >
                 {renderFunc}
                </PlacesAutocomplete> */}
                  {/* <CFormGroup>
                    <CLabel>Longitude</CLabel>
                    <CInput
                      type="text"
                      placeholder="Longitude"
                      name="longitude" 
                      value={formData.longitude}
                      onChange={handleInputChange}
                      required
                    />
                  </CFormGroup> */}
                  {/* <CFormGroup>
                    <CLabel>Latitude</CLabel>
                    <CInput
                      type="text"
                      placeholder="Latitude"
                      name="latitude" 
                      value={formData.latitude}
                      onChange={handleInputChange}
                      required
                    />
                  </CFormGroup> */}
                  {/* <CFormGroup>
                    <CLabel>Project ID</CLabel>
                    <CInput
                      type="text"
                      placeholder="Project ID"
                      name="projectId" 
                      value={formData.projectId}
                      onChange={handleInputChange}
                      required
                    />
                  </CFormGroup> */}
                  <CFormGroup>
                    <CLabel>Device Type</CLabel>
                    <CInput
                      type="text"
                      placeholder="Device Type"
                      name="deviceType" 
                      value={formData.deviceType}
                      onChange={handleInputChange}
                      required
                    />
                  </CFormGroup>
  
                  <button className="btn btn-dark btn-block"> <span>Submit</span></button>
                </CForm> 
                
            </CCardBody>
            </CCard>
  
           
            </CCol>
        </CRow>
      </CContainer>
    ) 
}

export default GateWayProvision



