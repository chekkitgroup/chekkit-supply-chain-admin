import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom"
import Select from 'react-select'
import Notification, { notifier } from 'react-toast-notifier';


import RestService from '../../_services/rest.service';

import {  
  CContainer, 
  CForm, 
  CCard, 
  CCardBody,
  CCol, 
 } from '@coreui/react'



const AssignTags = () => {
    const [company, setCompany] = useState([]);
    const { id } = useParams()
    const [rfid, setRfid] = useState();
    const [tags, setTags] = useState();

    const [selectRfid, setSelectRfids] = useState();
    const [selectCompany, setSelectCompany] = useState();

    console.log('tags', tags)

    const onSelectCompany = async (data) => {
      setSelectCompany(data.value)
    }

    const onSelectRF = async (data) => {
  
      var result = data.map(function (e) {
        return e.value;
      });


      setSelectRfids(result)
    }


          useEffect(() => {
            loadData(id);
            loadDatas(id);
        },[id])


              const loadData = async (id) => {
                RestService.getAllRfidTags(1,100).then(
                    (res) => {
                        setRfid(res.data);
                        console.log(res.data)
                        console.log(res.data[0].id)
                    },
                    error => {
                    }
                    );
            } 

            const loadDatas = async () => {
              RestService.getAllCompanies(1, 15).then(
                
                  (res) => {
                        setCompany(res.data);
                        console.log(res.data)


                    },
                    error => {
                    }
                    );
            } 

            let note = () => {
              notifier({ type: "success", message: "Tag Assigned", autoDismissTimeout:5000 });
            } 

            const handleSubmit = e => {
              e.preventDefault()
              let payload = {
                rfidIds: selectRfid,
              }
              
              RestService.AssignTags(selectCompany,payload).then(
                  (res) => {
                      setTags(res.data);
                      note()             
                  },
                  error => { 
                      notifier(error)

                  }
              );
          }


    return(
        <CContainer className="flex flex-column justify-center mt-12">
            <CCol  xs="10" sm="10" md="10" lg="10">
                <CCard className="">
                  <CCardBody className="">   
                      <CForm  onSubmit={handleSubmit}> 
                          <CCol md={12} lg={12}>
                              <CCard>
                              <Notification />

                                <CCardBody>
                                    <label>Company</label>
                                    {company && company.length > 0 && (
                                        <Select    onChange={onSelectCompany}
                                        closeMenuOnSelect={true}  options={company.map(com => {
                                          return {value: com.id, label: com.name} 
                                        })} 
                                        />
                                        )
                                      }

                                        <br />  <br />
                                        <label>RFID Tags</label>
                                      {/* {rfid && rfid.length > 0 && (
                                        <Select onChange={onSelectRF} closeMenuOnSelect={false} isMulti options={rfid.map(rf => {
                                          return {value: rf.id, label: rf.serialNumber + " " + "(" + rf.macAddress + ")"}
                                        })} 
                                        />
                                        )
                                      } */}
                                      {rfid && rfid.length > 0 && (
                                        <Select
                                          onChange={onSelectRF}
                                          closeMenuOnSelect={false}
                                          isMulti
                                          options={rfid.map((rf) => ({
                                            value: rf.id,
                                            label: `${rf.serialNumber} (${rf.macAddress})`,
                                          }))}
                                        />
                                      )}
                                </CCardBody>
                              </CCard>
                          </CCol>
                            <br />
                            <button className="btn btn-dark btn-block"> <span>Submit</span></button>
                      </CForm>   
                  </CCardBody>
                </CCard>
            </CCol>
        </CContainer>
    ) 
}

export default AssignTags