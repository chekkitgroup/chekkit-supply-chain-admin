import React, { useState, useEffect } from 'react'
import { useParams } from "react-router-dom"

import { CRow, 
  CContainer, 
  CForm, 
  CFormGroup, 
  CLabel, 
  CCard, 
  CInput, 
  CCardBody,
  CCol, 
 } from '@coreui/react'
import RestService from '../../_services/rest.service';

const CreateCompanyRoles = () => {
    
    const [roles, setRoles] = useState();
    const { id } = useParams() 

    console.log('roles', roles)


    const [formData, setFormData] = useState({
        name: '',
        companyId: id,
        permissions: []
    });





    useEffect(() => {
        loadData(id);
    },[id])

    const changeHandler = e => {
        setFormData({...formData, [e.target.name]: e.target.value})
    }
    
    const handleInputChange = e => {
      setFormData({...formData, [e.target.name]: [e.target.value]})
    };

 

    const loadData = async (id) => {
      RestService.getCompanyRoles(id).then(
          (res) => {
              setRoles(res.data);
              console.log(res.data)
          },
          error => {
    
          }
          );
  } 


    const clearForm = async () => {
        setFormData({
            name: '',
            permissions: '',
            companyId: ''
        });
    }

    const handleSubmit = e => {
        e.preventDefault()
        console.log(formData)

        RestService.createRoles(id, formData).then(
            (res) => {
                alert('Created Successfully')
                clearForm();
            }, 
            error => {
                console.log(error)
            }
        );
    }

    return(
      <CContainer>
        <CRow>
          <CCol xs="10" sm="10" md="10" lg="10">
          <CCard className="mx-4">
            <CCardBody className="p-4">   
              <CForm onSubmit={handleSubmit}>
                <CFormGroup>
                  <CLabel>Name</CLabel>
                  <CInput
                    type="text"
                    placeholder="Name"
                    name="name" 
                    value={formData.name}
                    onChange={changeHandler}
                    required
                  />
                </CFormGroup>
                <CFormGroup>
                  <CLabel>Permissions</CLabel>
                  <CInput
                    type="text"
                    placeholder="Permissions"
                    name="permissions" 
                    value={formData.permissions}
                    onChange={handleInputChange}
                    required
                  />
                </CFormGroup>

                <button className="btn btn-success btn-block"> <span>Submit</span></button>
              </CForm>    
              
          </CCardBody>
          </CCard>



          </CCol>
      </CRow>
    </CContainer>
  ) 
}

export default CreateCompanyRoles;