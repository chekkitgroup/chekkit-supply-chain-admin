import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom"
import RestService from '../../_services/rest.service';
import { useHistory } from 'react-router-dom';
import { CButton } from '@coreui/react'


const Permissions = () => {
    const [permissions, setPermissions] = useState([]);
    const { id } = useParams()
    const history = useHistory()

    const navigate = (id) => {
        history.push('/companies/ViewCompanies')
    }

    useEffect(() => {
        loadData(id);
        console.log(id)
    },[id])

    const loadData = async (id) => {
        RestService.getCompanyPermissions(id).then(
            (res) => {
                setPermissions(res.data);
                console.log(res.data)
            },
            error => {
                // const resMessage =
                // (error.response &&
                //     error.response.data &&
                //     error.response.data.message) ||
                // error.message ||
                // error.toString();
                // console.log(error)
                // this.setState({
                // loading: false,
                // message: resMessage
                // });
            }
            );
    } 
  

    return (
        <div>
            <div className="card">
                    {permissions && permissions.length > 0 && permissions.map((p, idx) => <div className="card-body m-1 px-6 py-3 text-left text-xs font-medium text-white-500 tracking-wider" key={idx}>{p}</div>)}
            </div>   


            <div>
                    <CButton style={{ backgroundColor: "#03303b", color:"#fff"}} shape="rounded-0" size="lg" className="m-1" onClick={() => navigate(id)} >Back</CButton>
            </div>

            
        </div>

        
    )
}

export default Permissions;




