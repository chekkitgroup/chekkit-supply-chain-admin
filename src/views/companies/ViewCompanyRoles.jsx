import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useParams } from "react-router-dom"
import RestService from '../../_services/rest.service';

import {  
    CButtonGroup,
    CButton
} from '@coreui/react'


const Roles = () => {
    const [roles, setRoles] = useState();
    const { id } = useParams()
    const { companyId } = useParams()
    const history = useHistory()


    console.log(id)
    console.log(companyId)

    const navigate = (id) => {
        history.push(`/companies/CreateCompanyRoles/${id}`)
    }

    const navigates = (id) => {
        history.push('/companies/ViewCompanies')
    }

    useEffect(() => {
        loadData(id);
        console.log(id)
    },[id])

   


    const loadData = async (id) => {
        RestService.getCompanyRoles(id).then(
            (res) => {
                setRoles(res.data);
            },
            error => {
                
            }
            );
    } 
  

    return (
 
        
        <div>
            <div className="card">
                    {roles && roles.length > 0 && roles.map((role, idx) => <div className="card-body" key={idx}>{role.name}</div>)}
            </div>   
            <div>
                <CButtonGroup> 
                    <CButton color="success" className="m-1" onClick={() => navigate(id)}>Create Roles</CButton>
                    <CButton style={{ backgroundColor: "#03303b", color:"#fff"}} shape="rounded-0" size="lg" className="m-1" onClick={() => navigates(id)} >Back</CButton>
                </CButtonGroup>  
            </div>       
        </div>
    )
}

export default Roles;




