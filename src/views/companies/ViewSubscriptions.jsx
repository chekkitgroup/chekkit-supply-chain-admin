import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useParams } from "react-router-dom"
import RestService from '../../_services/rest.service';

import {  
    CButton,
    CForm
} from '@coreui/react';

const ViewSubscriptions = () => {
    const [subscriptions, setSubscriptions] = useState([]);
    const [approve, setApprove] = useState();

    const { id } = useParams()
    const history = useHistory()

    const navigate = (id) => {
        history.push('/companies/ViewCompanies')
    }

    console.log(approve, 'approve')





    useEffect(() => {
        loadData(id);
        // console.log(id)
    },[id])

    // console.log(id)


    const handleSubmit = ({id, subId}) => {
        console.log(subId)
        console.log(id)
        RestService.ApproveSubscription(id, subId).then(
            (res) => {
                setApprove(res.data);

                alert('Approved Successfully')
            },
            error => {
                console.log(error)
            }
        );
    }


    const loadData = async (id) => {
        RestService.getCompanySubscriptions(id).then( 
            (res) => {
                setSubscriptions(res.data);
                console.log(res.data)
            },
            error => {
                // const resMessage =
                // (error.response &&
                //     error.response.data &&
                //     error.response.data.message) ||
                // error.message ||
                // error.toString();
                // console.log(error)
                // this.setState({
                // loading: false,
                // message: resMessage
                // });
            }
            );
    } 
    
    return (
        <div>
            <table>
                <tbody>
                <tr>
                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Type</th>
                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Description</th>
                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Price</th>
                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Months</th>
                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Status</th>
                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Action</th>
                </tr>
                {subscriptions && 
                    subscriptions.map((data, idx) => {
                        return (
                            <tr key={idx}>
                                <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.plan.type}</td>
                                <td className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.plan.description}</td>
                                <td className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.plan.price}</td>
                                <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.plan.numberOfMonths}</td>
                                <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.status}</td>
                                <td><CForm onSubmit={handleSubmit}> <CButton color="dark" shape="rounded-0" size="sm" className="m-1" onClick={handleSubmit.bind(null, { id:id, subId: data.id})} >Approve</CButton> </CForm></td>
                            </tr>
                        )
                    })
                }
                </tbody>
            </table>
            
            <div>
                <CButton style={{ backgroundColor: "#03303b", color:"#fff"}} shape="rounded-0" size="lg" className="m-1" onClick={() => navigate(id)} >Back</CButton>
            </div>      
    </div>
    )
}

export default ViewSubscriptions