import React, { useState } from 'react'
import Notification, { notifier } from 'react-toast-notifier';


import { CRow, 
  CContainer, 
  CForm, 
  CFormGroup, 
  CLabel, 
  CCard,  
  CInput, 
  CCardBody,
  CCol, 
  
 } from '@coreui/react'
import RestService from '../../_services/rest.service';
const RegisterTags = () => {
 
    const [formData, setFormData] = useState({
        macAddress: '',
        serialNumber: '',
        type: ''
    });
    
    const handleInputChange = e => {
      setFormData({...formData, [e.target.name]: e.target.value})
    };


    const clearForm = async () => {
        setFormData({
            macAddress: '',
            serialNumber: '',
            type: ''
        });
    }

    let note = () => {
      notifier({ type: "success", message: "Registered Successfully", autoDismissTimeout:5000 });
    } 


    const handleSubmit = e => {
        e.preventDefault()
        console.log(formData)

        let arr = [];

        let rfidDetails = formData;

        arr.push(rfidDetails)

        let payload = {
          rfidDetails : arr
        }


        console.log(arr)

        RestService.registerTags(payload).then(
            (res) => {
                note()
                clearForm();
            },
            error => {
              notifier(error)
                console.log(error)
            }
        );
    }


    return(
        <CContainer>
          <CRow>
            <CCol xs="10" sm="10" md="10" lg="10">
            <CCard className="mx-4">
            <Notification />
              <CCardBody className="p-4">   
                <CForm onSubmit={handleSubmit}>
                  <CFormGroup>
                    <CLabel>MAC Address</CLabel>
                    <CInput
                      type="text"
                      placeholder="MAC Address"
                      name="macAddress" 
                      value={formData.macAddress}
                      onChange={handleInputChange}
                      required
                    />
                  </CFormGroup>
                  <CFormGroup>
                    <CLabel>Serial Number</CLabel>
                    <CInput
                      type="text"
                      placeholder="Serial Number"
                      name="serialNumber" 
                      value={formData.serialNumber}
                      onChange={handleInputChange}
                      required
                    />
                  </CFormGroup>
                  <CFormGroup>
                    <CLabel>Type</CLabel>
                    <CInput
                      type="text"
                      placeholder="Type"
                      name="type" 
                      value={formData.type}
                      onChange={handleInputChange}
                      required
                    />
                  </CFormGroup>
  
                  <button className="btn btn-dark btn-block"> <span>Submit</span></button>
                </CForm>    
                
            </CCardBody>
            </CCard>
  
           
            </CCol>
        </CRow>
      </CContainer>
    ) 
}

export default RegisterTags