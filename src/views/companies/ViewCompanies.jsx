import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import RestService from '../../_services/rest.service';

import { 
    CDataTable,
    CButtonGroup,
    CButton
} from '@coreui/react'


const Companies = () => {
    const [company, setCompany] = useState([]);
    const history = useHistory()

    const navigate = (id) => {
        history.push(`/companies/ViewCompanyPermissions/${id}`)
    }

    const navigates = (id) => {
        history.push(`/companies/ViewCompanyRoles/${id}`)
    }

    const navigatez = (id) => {
        history.push(`/companies/ViewSubscriptions/${id}`)
    }

    const navigatea = (id) => {
        history.push(`/companies/ViewAssets/${id}`)
    }

    const navigateh = (id) => {
        history.push(`/companies/ViewHardware/${id}`)
    }

    const navigatef = (id) => { 
        history.push(`/companies/ViewHardware/${id}`)
    }

    useEffect(() => {
        loadData(); 
    },[])

    const loadData = async () => {
        RestService.getAllCompanies(1,15).then(
            (res) => {
                setCompany(res.data);
                console.log(res.data)
            },
            error => {
                
            }
            );
    } 
  

    return (
        <div>

            <CDataTable  items={company}
                        fields={[
                            {label : 'Company Name', key: 'name'},
                             {label: 'Country', key: 'country'}, {label: 'Address', key: 'address'},
                             {label: 'Actions', key: 'but'}
                        ]} hover striped itemsPerPage={100} clickableRows 
                            scopedSlots = {
                            {
                                'but':
                                    (item)=>(
                                      <td className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">
                                         <CButtonGroup> 
                                         <CButton style={{ backgroundColor: "#03303b", color:"#fff"}} color="default" className="m-1 px-6 py-3 text-left text-xs font-medium text-white-500 tracking-wider" onClick={() => navigate(item.id)}>View Permissions</CButton> 
                                         <CButton style={{ backgroundColor: "#03303b", color:"#fff"}} color="default" className="m-1 px-6 py-3 text-left text-xs font-medium text-white-500 tracking-wider" onClick={() => navigates(item.id)}>View Roles</CButton> 
                                         <CButton style={{ backgroundColor: "#03303b", color:"#fff"}} color="default" className="m-1 px-6 py-3 text-left text-xs font-medium text-white-500 tracking-wider" onClick={() => navigatez(item.id)}>View Subscription Logs</CButton> 
                                         <CButton style={{ backgroundColor: "#03303b", color:"#fff"}} color="default" className="m-1 px-6 py-3 text-left text-xs font-medium text-white-500 tracking-wider" onClick={() => navigateh(item.id)}>View Hardware</CButton> 
                                         <CButton style={{ backgroundColor: "#03303b", color:"#fff"}} color="default" className="m-1 px-6 py-3 text-left text-xs font-medium text-white-500 tracking-wider" onClick={() => navigatea(item.id)}>View Assets</CButton> 
                                         <CButton style={{ backgroundColor: "#03303b", color:"#fff"}} color="default" className="m-1 px-6 py-3 text-left text-xs font-medium text-white-500 tracking-wider" onClick={() => navigatef(item.id)}>RFID Tags</CButton> 
                                        </CButtonGroup>
                                          
                                  </td>
                            ),
                            }
                        }
                    /> 
        </div>
    )
}

export default Companies;




