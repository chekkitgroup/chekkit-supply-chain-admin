import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom"
import RestService from '../../_services/rest.service';



const ViewRfid = () => {
    const [rifd, setRfid] = useState();
    const { id } = useParams()
  



    useEffect(() => {
        loadData(id);
    },[id])

 


    const loadData = async () => {
        RestService.getAllRfidTags().then(
            (res) => {
                setRfid(res.data);
                console.log(res.data)
            },
            error => {
          
            }
            );
    } 

    
  

    return (
 
        
        <div>
           <div className="card">
                <table className="table table-striped table-hover">
                    <tbody>
                    <tr>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">MAC Address</th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Serial Number</th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Type</th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Assigned</th>
                    </tr>
                    {rifd && 
                        rifd.map((data, idx) => {

                            return (
                                <tr key={idx}>
                                    <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.macAddress}</td>
                                    <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.serialNumber}</td>
                                    <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.type}</td>
                                    <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.isAssigned}</td>
                                </tr>
                            )
                        })
                    } 
                    </tbody>
                </table>
            </div>       
        </div>
    )
}

export default ViewRfid;




