import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useParams } from "react-router-dom"
import RestService from '../../_services/rest.service';

import { 
    CButton
} from '@coreui/react'


const ViewAssets = () => {
    const [assets, setAssets] = useState();
    const { id } = useParams()
    const history = useHistory()

    const navigate = (id) => {
        history.push('/companies/ViewCompanies')
    }


    useEffect(() => {
        loadData(id);
    },[id])



    const loadData = async (id) => {
        RestService.getAssets(id,1,100).then(
            (res) => {
                setAssets(res.data);
                console.log(res.data)
            },
            error => {
         
            }
            );
    } 

    
  

    return (
 
        
        <div>
            <div className="card">
                <table className="table table-striped table-hover">
                    <tbody>
                    <tr>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Name</th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Price</th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Package Level</th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Batch Number</th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Serial Number</th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Category</th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Date Created</th>
                    </tr>
                    {assets && 
                        assets.map((data, idx) => {
                            var cts = data.created_at,
                            cdate = (new Date(cts)).toLocaleString();
                            return (
                                <tr key={idx}>
                                    <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.name}</td>
                                    <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.price}</td>
                                    <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.packageLevel}</td>
                                    <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.batchNumber}</td>
                                    <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.serialNumber}</td>
                                    <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.category}</td>
                                    <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{cdate}</td>
                                </tr>
                            )
                        })
                    }
                    </tbody>
                </table>
                <div>
                    <CButton style={{ backgroundColor: "#03303b", color:"#fff"}} shape="rounded-0" size="lg" className="m-1" onClick={() => navigate(id)} >Back</CButton>
            </div>
            </div>        
        </div>
    )
}

export default ViewAssets;




