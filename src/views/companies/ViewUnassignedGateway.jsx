import React, { useState, useEffect } from 'react';
// import { useHistory } from 'react-router-dom';
import Select from 'react-select'
import RestService from '../../_services/rest.service';
import Notification, { notifier } from 'react-toast-notifier';



import { 
    CButton,  
    CLabel,
    CFormGroup,
    CForm,
    CInput
} from '@coreui/react'







const ViewUnassignedGateway = ({data}) => {
    const [gateway, setGateway] = useState();
    const [company, setCompany] = useState([]);
    const [deprovision, setDeprovision] = useState();
    const [showModal, setShowModal] = useState(false);
    const [selectCompany, setSelectCompany] = useState();
    const [formData, setFormData] = useState({
        companyId: '',
        location: '',
        macAddress: '',
        deviceType: ''
    });

    console.log(deprovision, 'provision')
    console.log(selectCompany, 'selectCompany')
    console.log(selectCompany, 'selectCompany')

  

    // const history = useHistory()


    // const navigate = (id) => {
    //     history.push('/companies/GateWayProvision/${id}')
    //     console.log(deprovision, 'provision')
    //     console.log(selectCompany, 'selectCompany')
    // }

    const handleInputChange = e => {
        setFormData({...formData, [e.target.name]: e.target.value})
      };


    useEffect(() => {
        loadData();
        loadDatas();
    }, [])

    const loadDatas = async () => {
        RestService.getAllCompanies(1, 15).then(
          
            (res) => {
                setCompany(res.data);
            },
            error => {
            }
            );
    } 

    const onSelectCompany = async (data) => {
        setSelectCompany(data.value)
        let companyId = data.value;
        setFormData({...formData, companyId})
      }

      let note = () => {
        notifier({ type: "success", message: "Provisioned Successfully", autoDismissTimeout:5000 });
      } 

    const handleSubmit = ({macAddress}) => {
        console.log(macAddress)
        RestService.deprovisionGateway(macAddress).then(
            (res) => {
                setDeprovision(res.data);
                notifier('Deprovisioned Successfully')
            },
            error => {
                console.log(error)
                notifier(error)

            }
        );
    }


    const loadData = async () => {
        RestService.getUnassignedGateway(1,15).then(
            (res) => {
                setGateway(res.data[0]);
            },
            error => {
        
            }
            );
    } 

    // const body = {
    //     location: formData.location,
    //     macAddress: formData.macAddress,
    //     deviceType: formData.deviceType,
    // }

    const submitForm = e => {
        e.preventDefault()
        console.log(formData)

        RestService.gatewayProvision(formData).then(
            (res) => {
                note()
                clearForm();
            },
            error => {
                console.log(error)
                notifier(error)
            }
        );
    } 


    const clearForm = async () => {
        setFormData({
          companyId: '',
          location: '',
          macAddress: '',
          deviceType: ''
        });
    }

    
  

   
    return (

        
        <div>
           <div className="card">

                <table className="table table-striped table-hover">
                    <tbody>
                    <tr>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">MAC Address</th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Serial Number</th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Assigned</th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Action </th>
                    </tr>
                    {gateway && 
                        gateway.map((data, idx) => {
                            return (
                                <tr key={idx}>
                                    <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.macAddress}</td>
                                    <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{data.serialNumber}</td>
                                    <td  className="px-6 py-3 text-left text-xs font-medium text-gray-500 tracking-wider">{ String(data.isAssigned) !== 'true' ? 'false' : 'true' }</td>
                                    <td><CForm onSubmit={handleSubmit}> { data.isAssigned === true ? <CButton style={{ backgroundColor: "red", color:"#fff"}} shape="rounded-0" size="sm" className="m-1" onClick={handleSubmit.bind(null, { macAddress:data.macAddress})}>Deprovision</CButton> :  <CButton style={{ backgroundColor: "green", color:"#fff"}} shape="rounded-0" size="sm" className="m-1" onClick={()=> {setFormData({...formData, macAddress:data.macAddress,location:data.location, deviceType:data.deviceType }); setShowModal(true)} }>Provision</CButton>}  </CForm></td>
                                </tr>
                            )
                        })
                    } 
                    </tbody>
                </table>


                <Notification />

                    
                         {showModal ? (
                            <div className="App h-screen flex flex-col items-center justify-center bg-black-200">
                                <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                                    <div className="relative w-auto my-6 mx-auto max-w-3xl">
                                        <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white text-black outline-none focus:outline-none">
                                           <div className="flex items-start justify-between p-5 border-b border-solid border-gray-300 rounded-t ">
                                               <h3 className="text-3xl mt-8 font-bold">Gateway Provisioning</h3>
                                                 <button
                                                     className="bg-transparent border-0 text-black p-1 float-right"
                                                     onClick={() => setShowModal(false)}>
                                                      <span className="text-black opacity-7 m-2 h-7 w-6 text-xl block bg-red-400 py-0 rounded-full">
                                                        x
                                                      </span>
                                                 </button>
                                            </div>
                                            <div className="relative p-6 flex-auto">

                                                <CForm className="bg-gray-200 shadow-md rounded px-8 pt-6 pb-8 w-full" onSubmit={submitForm}>
                                                    <CFormGroup>
                                                        <CLabel className="block text-black text-sm font-bold mb-1">Company</CLabel>
                                                                {company && company.length > 0 && (
                                                                    <Select name="companyId" className="text-black"  onChange={onSelectCompany} closeMenuOnSelect={true}  options={company.map(com => {
                                                                        return {value: com.id, label: com.name} })} 
                                                                    />
                                                                 )}
                                                    </CFormGroup>
                                                    <CFormGroup>
                                                        <CLabel className="block text-black text-sm font-bold mb-1">Mac Address</CLabel>
                                                        <CInput
                                                            type="text"
                                                            placeholder="Mac Address"
                                                            name={"macAddress"}
                                                            value={formData.macAddress}
                                                            onChange={handleInputChange}
                                                            required
                                                        />
                                                    </CFormGroup>
                                                    <CFormGroup>
                                                        <CLabel className="block text-black text-sm font-bold mb-1">Location</CLabel>
                                                        <CInput
                                                            type="text"
                                                            placeholder="Location"
                                                            name="location" 
                                                            value={formData.location || ''}
                                                            onChange={handleInputChange}
                                                            required
                                                            />
                                                    </CFormGroup>
                                                    <CFormGroup>
                                                        <CLabel className="block text-black text-sm font-bold mb-1">Device Type</CLabel>
                                                        <CInput
                                                            type="text"
                                                            placeholder="Device Type"
                                                            name="deviceType" 
                                                            value={formData.deviceType}
                                                            onChange={handleInputChange}
                                                            required
                                                        />
                                                    </CFormGroup>
                                                                        
                                                </CForm>
                                            </div>
                                            <div className="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b">
                                                <button
                                                    className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1"
                                                    type="button"
                                                    onClick={() => setShowModal(false)}
                                                >
                                                    Close
                                                </button>
                                                <button
                                                    className="text-white bg-green-500 active:bg-green-700 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
                                                    type="button"
                                                    onClick={submitForm}
                                                >
                                                    Submit
                                                </button>


                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        ) : null}
            </div>       
        </div>

     
    )
}

export default ViewUnassignedGateway;




