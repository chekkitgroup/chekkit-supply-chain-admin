import React from 'react';

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const Plans = React.lazy(() => import('./views/plans/Plans'));
const ViewCompanies = React.lazy(() => import('./views/companies/ViewCompanies'));
const ViewCompanyUsers = React.lazy(() => import('./views/companies/ViewCompanyUsers'));
const ViewCompanyRoles = React.lazy(() => import('./views/companies/ViewCompanyRoles'));
const ViewCompanyPermissions = React.lazy(() => import('./views/companies/ViewCompanyPermissions'));
const CreateCompanyRoles = React.lazy(() => import('./views/companies/CreateCompanyRoles'));
const CreateCompanyPermissions = React.lazy(() => import('./views/companies/CreateCompanyPermissions'));
const ViewSubscriptions = React.lazy(() => import('./views/companies/ViewSubscriptions'));

const ViewHardware = React.lazy(() => import('./views/companies/ViewHardware'));
const ViewAssets = React.lazy(() => import('./views/companies/ViewAssets'));


const ViewRfid = React.lazy(() => import('./views/companies/ViewRfid'));
const RegisterTags = React.lazy(() => import('./views/companies/RegisterTags'));
const AssignTags = React.lazy(() => import('./views/companies/AssignTags'));
const GateWayProvision = React.lazy(() => import('./views/companies/GateWayProvision'));
const ViewUnassignedGateway = React.lazy(() => import('./views/companies/ViewUnassignedGateway'));


const Home = React.lazy(() => import('./views/home/Home')); 


const routes = [

  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/plans/Plans', exact: true, name: 'Plans', component: Plans },
  { path: '/companies/ViewCompanies', exact: true, name: 'ViewCompanies', component: ViewCompanies },
  { path: '/companies/ViewCompanyUsers', exact: true, name: 'ViewCompanyUsers', component: ViewCompanyUsers },
  { path: '/companies/ViewCompanyRoles/:id', exact: true, name: 'ViewCompanyRoles', component: ViewCompanyRoles },
  { path: '/companies/ViewCompanyPermissions/:id', exact: true, name: 'ViewCompanyPermissions', component: ViewCompanyPermissions },
  { path: '/companies/CreateCompanyRoles/:id', exact: true, name: 'CreateCompanyRoles', component: CreateCompanyRoles },
  { path: '/companies/CreateCompanyPermissions', exact: true, name: 'CreateCompanyPermissions', component: CreateCompanyPermissions },
  { path: '/companies/ViewSubscriptions/:id', exact: true, name: 'ViewSubscriptions', component: ViewSubscriptions },

  { path: '/companies/ViewHardware/:id', exact: true, name: 'ViewHardware', component: ViewHardware },
  { path: '/companies/ViewAssets/:id', exact: true, name: 'ViewAssets', component: ViewAssets },


  { path: '/companies/ViewRfid', exact: true, name: 'ViewRfid', component: ViewRfid },
  { path: '/companies/RegisterTags', exact: true, name: 'RegisterTags', component: RegisterTags },
  { path: '/companies/AssignTags', exact: true, name: 'AssignTags', component: AssignTags },
  { path: '/companies/GateWayProvision', exact: true, name: 'GateWayProvision', component: GateWayProvision },
  { path: '/companies/ViewUnassignedGateway', exact: true, name: 'ViewUnassignedGateway', component: ViewUnassignedGateway },



  { path: '/views/home/Home/:id', exact: true, name: 'Home', component: Home },


];

export default routes;
