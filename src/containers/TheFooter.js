import React from 'react'
import { CFooter } from '@coreui/react'

const TheFooter = () => {
  return (
    <CFooter fixed={false}>
      <div>
        <a href="www.chekkitapp.com" rel="noopener noreferrer">Chekkit Technologies</a>
        <span className="ml-1">&copy; 2023</span>
      </div>
      <div className="mfs-auto">
        <span className="mr-1">Powered by</span>
        <a href="www.chekkitapp.com" target="_blank" rel="noopener noreferrer">Chekkit Technologies</a>
      </div>
    </CFooter>
  )
}

export default React.memo(TheFooter)
