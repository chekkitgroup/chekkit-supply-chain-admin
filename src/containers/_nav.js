import React from 'react'
import CIcon from '@coreui/icons-react'

const _nav =  [
  {
    _tag: 'CSidebarNavItem',
    name: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>
  },
  // {
  //   _tag: 'CSidebarNavItem',
  //   name: 'Plans',
  //   to: '/plans/plans',
  //   icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>,
  //   // badge: {
  //   //   color: 'info',
  //   //   text: 'NEW',
  //   // }
  // },
  // {
  //   _tag: 'CSidebarNavItem',
  //   name: 'Home',
  //   route: '/home/Home',
  //   icon: 'cil-speedometer',
  // },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Companies',
    route: '/companies',
    icon: 'cil-calculator',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Companies',
        to: '/companies/ViewCompanies',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'RFID',
        to: '/companies/ViewRfid',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Register Tags',
        to: '/companies/RegisterTags',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Assign Tags',
        to: '/companies/AssignTags',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Gateway Provisioning',
        to: '/companies/GateWayProvision',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Unassigned Gateway',
        to: '/companies/ViewUnassignedGateway',
      },
      // {
      //   _tag: 'CSidebarNavItem',
      //   name: 'Get Company User',
      //   to: '/companies/ViewCompanyUsers',
      // },
      // {
      //   _tag: 'CSidebarNavItem',
      //   name: 'Create Company Roles',
      //   to: '/companies/CreateCompanyRoles',
      // },
      // {
      //   _tag: 'CSidebarNavItem',
      //   name: 'Create Company Permissions',
      //   to: '/companies/CreateCompanyPermissions',
      // },
      // {
      //   _tag: 'CSidebarNavItem',
      //   name: 'Get Company Subscriptions',
      //   to: '/companies/ViewSubscriptions',
      // },
      // {
      //   _tag: 'CSidebarNavItem',
      //   name: 'Get Company Permission',
      //   to: '/companies/ViewCompanyPermissions',
      // },
      ],
  },
  // {
  //   _tag: 'CSidebarNavTitle',
  //   _children: ['Theme']
  // },
  // {
  //   _tag: 'CSidebarNavItem',
  //   name: 'Colors',
  //   to: '/theme/colors',
  //   icon: 'cil-drop',
  // },
  // {
  //   _tag: 'CSidebarNavItem',
  //   name: 'Typography',
  //   to: '/theme/typography',
  //   icon: 'cil-pencil',
  // },
  // {
  //   _tag: 'CSidebarNavTitle',
  //   _children: ['Components']
  // },
  // {
  //   _tag: 'CSidebarNavDropdown',
  //   name: 'Consumer Intelligence',
  //   route: '/consumer-intelligence',
  //   icon: 'cil-puzzle',
  //   _children: [
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Create Batches and Products',
  //       to: '/consumer-intelligence/create-batches',
  //     },
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Generate/Request Sticker Labels',
  //       to: '/consumer-intelligence/generate-labels',
  //     },
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Setup/Manage Survey Questions',
  //       to: '/consumer-intelligence/setup-survey',
  //     },
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Setup/Manage Rewards',
  //       to: '/consumer-intelligence/setup-rewards',
  //     },
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Reports',
  //       to: '/consumer-intelligence/reports',
  //     },
  //   ],
  // },
  // {
  //   _tag: 'CSidebarNavDropdown',
  //   name: 'Connect+',
  //   route: '/connect',
  //   icon: 'cil-cursor',
  //   _children: [
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'View Feed',
  //       to: '/connect/view-feed',
  //     },
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Chat/Messages',
  //       to: '/connect/chat',
  //     },
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Order Management',
  //       to: '/connect/order-management',
  //     },
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'My Profile',
  //       to: '/connect/my-profile',
  //     },
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Manage Product Listings',
  //       to: '/connect/product-listings',
  //     },
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Invoices & Payment',
  //       to: '/connect/invoices',
  //     },
  //   ],
  // },
  // {
  //   _tag: 'CSidebarNavDropdown',
  //   name: 'Engage',
  //   route: '/engage',
  //   icon: 'cil-ban',
  //   _children: [
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Contacts',
  //       to: '/engage/contacts',
  //     },
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Data-Capturing',
  //       to: '/engage/contacts',
  //     },
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Survey Centre',
  //       to: '/engage/survey-centre',
  //     },
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Reward Centre',
  //       to: '/engage/reward-centre',
  //     },
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'View/Sort Winners',
  //       to: '/engage/view-winners',
  //     },
  //     ],
  // },
  // {
  //   _tag: 'CSidebarNavDropdown',
  //   name: 'Asset Management',
  //   route: '/asset-management',
  //   icon: 'cil-chart-pie',
  //   _children: [
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Asset Tracking & Management',
  //       to: '/asset-management/asset-tracking',
  //     },
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Inventory Management',
  //       to: '/asset-management/inventory-management',
  //     },
  //     ],
  // },
  // {
  //   _tag: 'CSidebarNavDropdown',
  //   name: 'Warehouse',
  //   route: '/warehouse',
  //   icon: 'cil-calculator',
  //   _children: [
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Get Warehouse',
  //       to: '/warehouse/get-warehouse',
  //     },
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Add Warehouse',
  //       to: '/warehouse/add-warehouse',
  //     },
  //     ],
  // },
  // {
  //   _tag: 'CSidebarNavDropdown',
  //   name: 'Reports',
  //   route: '/reports',
  //   icon: 'cil-bell',
  //   _children: [
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'View Report Per Activity',
  //       to: '/asset-management/asset-tracking',
  //     },
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Report Generation History',
  //       to: '/asset-management/inventory-management',
  //     },
  //     ],
  // },
  // {
  //   _tag: 'CSidebarNavDropdown',
  //   name: 'Hardware',
  //   route: '/hardware',
  //   icon: 'cil-star',
  //   _children: [
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Get Hardware',
  //       to: '/hardware/get-hardware',
  //     },
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Order Hardware',
  //       to: '/hardware/order-hardware',
  //     }
  //     ],
  // },
]

export default _nav
