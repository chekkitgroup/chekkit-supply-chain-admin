import React, { Component } from 'react'
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import AuthService from '../_services/authentication.service';
import PropTypes from 'prop-types';
import { Redirect } from "react-router";

export default class TheHeaderDropdown extends Component {
  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this);

    this.state = {
      username: "",
      redirect: false
    };
  }

   logout(){
    AuthService.logout();

    this.setState({
      redirect: true
    });
  }

  render() {
    var _this = this;
  return (

    <CDropdown
      inNav
      className="c-header-nav-items mx-2"
      direction="down"
    >
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <div className="c-avatar">
          <CImg
            src={'avatars/6.jpg'}
            className="c-avatar-img"
            alt="Log Out"
          />
        </div>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownItem
          header
          tag="div"
          color="light"
          className="text-center"
        >
          <strong>Settings</strong>
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-user" className="mfe-2" />Profile
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-settings" className="mfe-2" /> 
          Settings
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-credit-card" className="mfe-2" /> 
          Payments
        </CDropdownItem>
        <CDropdownItem divider />
        <CDropdownItem onClick={() => _this.logout()}>
          <CIcon name="cil-lock-locked" className="mfe-2" /> 
          Log out
        </CDropdownItem>
      </CDropdownMenu>

      {(this.state.redirect )&& (<Redirect to="/login" />)}

    </CDropdown>
    
  )
  }
}

TheHeaderDropdown.contextTypes = {
  router: PropTypes.object
};


